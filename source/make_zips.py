#!/usr/bin/env python3.8
import json
import glob
import os
import shutil


rows = []
for d in glob.glob('*/'):
    with open(d+"data.json") as f: j = json.load(f)
    name = j['name']
    fname = f'../public/{name}'
    zipfile = f"{fname}.zip"
    if not os.path.exists(zipfile):
        shutil.make_archive(fname, 'zip', d)
    rows.append(f"""<tr><td> <a href="{name}.zip">{name}.zip</a></td><td> {j["date"]} </td><td> {j["desc"]} </td></tr>""")
with open("../public/index.html","w") as outf:
  with open("index.html") as f:
    txt = f.read()
    files = "\n                ".join(rows)
    outf.write(txt.replace("{{.Rows}}", files))
